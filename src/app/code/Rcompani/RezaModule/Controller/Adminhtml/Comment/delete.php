<?php declare(strict_types=1);

namespace Rcompani\RezaModule\Controller\Adminhtml\Comment;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultFactory;
use Rcompani\RezaModule\Model\ResourceModel\Comment as CommentResource;
use Rcompani\RezaModule\Model\CommentFactory;

class delete extends Action

{
    protected $commentFactory;

    protected $commentResource;

    public function __construct(
        CommentResource $commentResource,
        CommentFactory $commentFactory,
        Context $context)
    {
        $this->commentResource = $commentResource;
        $this->commentFactory = $commentFactory;
        parent::__construct($context);
    }


    public function execute()
    {
        try{
            $id = $this->getRequest()->getParam('id');
            $faq = $this->commentFactory->create();
            $this->commentResource->load($faq, $id);
            if ($faq->getId()) {
                $this->commentResource->delete($faq);
                $this->messageManager->addSuccessMessage(__('The record has been deleted.'));
            } else {
                $this->messageManager->addErrorMessage(__('The record does not exist.'));
            }
        } catch (\Exception $exception) {
            $this->messageManager->addErrorMessage($exception->getMessage());
        }

        /** @var Redirect $redirect */
        $redirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        return $redirect->setPath('*/*');
    }
}
