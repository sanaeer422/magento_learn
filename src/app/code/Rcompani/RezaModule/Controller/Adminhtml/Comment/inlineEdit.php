<?php declare(strict_types=1);

namespace Rcompani\RezaModule\Controller\Adminhtml\Commnet;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Rcompani\RezaModule\Model\Comment;
use Rcompani\RezaModule\Model\CommentFactory;
use Rcompani\RezaModule\Model\ResourceModel\Comment as CommentResource;

class InlineEdit extends Action implements HttpPostActionInterface
{
//    const ADMIN_RESOURCE = 'Macademy_Minerva::faq_save';

    protected $jsonFactory;

    protected $commentFactory;

    protected $commentResource;


    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        CommentFactory $commentFactory,
        CommentResource $commentResource
    ) {
        parent::__construct($context);
        $this->jsonFactory = $jsonFactory;
        $this->commentFactory = $commentFactory;
        $this->commentResource = $commentResource;
    }


    public function execute()

    {

        $json = $this->jsonFactory->create();
        $messages = [];
        $error = false;
        $isAjax = $this->getRequest()->getParam('isAjax', false);
        $items = $this->getRequest()->getParam('items', []);

        if (!$isAjax || !count($items)) {
            $messages[] = __('Please correct the data sent.');
            $error = true;
        }

        if (!$error) {
            foreach ($items as $item) {
                $id = $item['id'];
                try {
                    $comment = $this->commentFactory->create();
                    $this->commentResource->load($comment, $id);
                    $comment->setData(array_merge($comment->getData(), $item));
                    $this->commentResource->save($comment);
                } catch (\Exception $e) {
                    $messages[] = __("Something went wrong while saving item $id");
                    $error = true;
                }
            }
        }

        return $json->setData([
            'messages' => $messages,
            'error' => $error,
        ]);
    }
}
