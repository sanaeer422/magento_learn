<?php

namespace Rcompani\RezaModule\Controller\Adminhtml\Comment;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class index extends Action

{
    protected $pageFactory;

    public function __construct(
        Context $context,
        PageFactory $pageFactory
    ) {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
    }
    public function execute(): \Magento\Framework\View\Result\Page
    {
        $page = $this->pageFactory->create();
        $page->setActiveMenu('Rcompani_RezaModule::comment');
        $page->getConfig()->getTitle()->prepend(__('Comment'));

        return $page;
    }
}
