<?php declare(strict_types=1);

namespace Rcompani\RezaModule\Controller\Adminhtml\Comment;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Ui\Component\MassAction\Filter;
use Rcompani\RezaModule\Model\ResourceModel\Comment\CollectionFactory;

class massDelete extends Action

{
    protected $collectionFactory;

    protected $filter;

    public function __construct(
        CollectionFactory $collectionFactory,
        Filter $filter,
        Context $context
    )
    {
        parent::__construct($context);
        $this->collectionFactory = $collectionFactory;
        $this->filter = $filter;
    }


    public function execute()
    {
        $collection = $this->collectionFactory->create();
        $items = $this->filter->getCollection($collection);
        $itemSize = $items->getSize();

        foreach ($items as $item) {
            $item->delete();
        }

        $this->messageManager->addSuccessMessage(__('A total of %1 record has been deleted', $itemSize));

        $redirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        return $redirect->setPath('*/*');

    }
}
