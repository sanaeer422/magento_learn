<?php declare(strict_types=1);

namespace Rcompani\RezaModule\Controller\Adminhtml\Comment;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\View\Result\PageFactory;

class NewAction extends Action implements HttpGetActionInterface

{
    protected $pageFactory;

    public function __construct(
        PageFactory $pageFactory,
        Context $context)
    {
        $this->pageFactory = $pageFactory;
        parent::__construct($context);
    }

    public function execute(): Page
    {
        $page = $this->pageFactory->create();
        $page->setActiveMenu('Rcompani_RezaModule::comment');
        $page->getConfig()->getTitle()->prepend(__('New Comment'));

        return $page;
    }
}
