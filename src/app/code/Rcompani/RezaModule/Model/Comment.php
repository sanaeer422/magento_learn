<?php

namespace Rcompani\RezaModule\Model;

use Magento\Framework\Model\AbstractModel;

class Comment extends AbstractModel

{
    protected function _construct()
    {
        $this->_init(ResourceModel\Comment::class);
    }
}
