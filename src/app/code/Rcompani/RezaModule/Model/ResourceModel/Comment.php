<?php

namespace Rcompani\RezaModule\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Comment extends AbstractDb

{
    const MAIN_TABLE = 'RezaModule_Comment';
    const ID_FIELD_NAME = 'id';

    public function _construct()
    {
        $this->_init(self::MAIN_TABLE,self::ID_FIELD_NAME);
    }
}
