<?php

namespace Rcompani\RezaModule\Model\ResourceModel\Comment;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Rcompani\RezaModule\Model\Comment;

class Collection extends AbstractCollection

{
    public function _construct()
    {
        $this->_init(Comment::class,\Rcompani\RezaModule\Model\ResourceModel\Comment::class);
    }
}
