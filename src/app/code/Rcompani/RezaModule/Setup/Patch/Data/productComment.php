<?php

namespace Rcompani\RezaModule\Setup\Patch\Data;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Rcompani\RezaModule\Model\ResourceModel\Comment;

class productComment implements DataPatchInterface

{
    protected $ModuleDataSetup;

    protected $resource;


    public function __construct(
        ModuleDataSetupInterface $ModuleDataSetup,
        ResourceConnection $resource

    )
    {
        $this->ModuleDataSetup = $ModuleDataSetup;
        $this->resource = $resource;
    }
    public static function getDependencies()
    {
        return [];
    }

    /**
     * Get aliases (previous names) for the patch.
     *
     * @return string[]
     */
    public function getAliases()
    {
        return [];
    }
    public function apply()
    {
        $connection = $this->resource->getConnection();
        $data = [
            [
                'product' => 'kafsh gol goli',
                'comment' => 'best shoes ever',
                'is_published' => 1,
            ],
            [
                'product' => 'pato palangi',
                'comment' => 'such fucking pato',
                'is_published' => 0,
            ],
            [
                'product' => 'hansferi',
                'comment' => 'good hansferi',
                'is_published' => 1,
            ],
        ];
        $connection->insertMultiple(Comment::MAIN_TABLE,$data);
        return $this;
    }
}
