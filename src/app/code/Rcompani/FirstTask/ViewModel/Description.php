<?php

namespace Rcompani\FirstTask\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;

class Description implements ArgumentInterface

{
    public function getWithWordCount()
    {
        $description = __('hy bitch');
        $wordCount = str_word_count($description);
        return "$description <em>($wordCount words)</em>";
    }
}
