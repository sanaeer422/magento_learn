<?php declare(strict_types=1);

namespace Rcompani\FirstTask\Controller\Adminhtml\CustomerData;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class indexas extends Action

{
    protected $pageFactory;

    protected $curl;

    public function __construct(
        PageFactory $pageFactory,
        Context $context,
        \Magento\Framework\HTTP\Client\Curl $curl
    ) {
        $this->pageFactory = $pageFactory;
        $this->curl = $curl;
        parent::__construct($context);
    }
    public function execute()
    {
        $page = $this->pageFactory->create();
        $page->getConfig()->getTitle()->prepend(__('Customer ssssData'));


        return $page;
    }

}
