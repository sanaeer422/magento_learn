<?php


namespace Rcompani\FirstTask\Controller\Adminhtml\CustomerData;

use Magento\Framework\App\Action\Action;
use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\App\Action\Context;


class Curl extends Action

{
    protected $_curl;

    protected $pageFactory;


    /**
     * @param Context $context
     * @param \Magento\Framework\HTTP\Client\Curl $curl
     */
    public function __construct(
        Context $context,
        \Magento\Framework\HTTP\Client\Curl $curl,
        PageFactory $pageFactory
    ) {
        $this->pageFactory = $pageFactory;
        $this->_curl = $curl;
        parent::__construct($context);
    }

    public function execute()
    {
        $page = $this->pageFactory->create();
        $page->getConfig()->getTitle()->prepend(__('page'));
        //if the method is get
        $uri = 'https://reqres.in/api/users?page=2';
        $this->_curl->get($uri);
        //response will contain the output in form of JSON string
        $response = $this->_curl->getBody();

        return $response;
    }


}

//namespace Rcompani\FirstTask\Controller\Adminhtml\CustomerData;
//use Magento\Backend\App\Action;
//
//
//class Curl extends Action
//
//
//
//{
//    protected $_curl;
//
//    /**
//     * @param Context$context
//     * @param \Magento\Framework\HTTP\Client\Curl $curl
//     */
//    public function __construct(
//        Context $context,
//        \Magento\Framework\HTTP\Client\Curl $curl
//    ) {
//        $this->_curl = $curl;
//        parent::__construct($context);
//    }
//
//    public function execute()
//    {
//
//        $this->_curl->get('reqres.in/api/users?page=2');
//        $response = $this->_curl->getBody();
//
//        return $response;
//    }
//
//
//}
