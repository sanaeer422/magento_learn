<?php

namespace Rcompani\FirstTask\Controller\Adminhtml\CustomerData;

use Magento\Framework\App\Action\Action;
use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\App\Action\Context;


class index extends Action

{
    protected $_curl;

    protected $pageFactory;


    /**
     * @param Context $context
     * @param \Magento\Framework\HTTP\Client\Curl $curl
     */
    public function __construct(
        Context $context,
        \Magento\Framework\HTTP\Client\Curl $curl,
        PageFactory $pageFactory
    ) {
        $this->pageFactory = $pageFactory;
        $this->_curl = $curl;
        parent::__construct($context);
    }

    public function execute()
    {
        $page = $this->pageFactory->create();
        $page->getConfig()->getTitle()->prepend(__('page'));
        //if the method is get
        $uri = __('reqres.in/api/users?page=2');
        $this->_curl->get($uri);
        //response will contain the output in
        // form of JSON string

        $response = $this->_curl->getBody();

        return $page;
    }


}//declare(strict_types=1);
//
//namespace Rcompani\FirstTask\Controller\Adminhtml\CustomerData;
//
//use Magento\Backend\App\Action;
//use Magento\Backend\App\Action\Context;
//use Magento\Framework\View\Result\PageFactory;
//
//class index extends Action
//
//{
//    protected $pageFactory;
//
//    public function __construct(
//        PageFactory $pageFactory,
//        Context $context)
//    {
//        $this->pageFactory = $pageFactory;
//
//        parent::__construct($context);
//    }
//    public function execute()
//    {
//        $page = $this->pageFactory->create();
//        $page->getConfig()->getTitle()->prepend(__('Customer Data'));
//
//        return $page;
//    }
//
//}
