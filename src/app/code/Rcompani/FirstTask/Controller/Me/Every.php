<?php

namespace Rcompani\FirstTask\Controller\Me;

use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\ResultFactory;

class Every extends Action

{
    public function execute()
    {
        return $this->_redirect('my-selling/me/raw',[
            'limit' =>222,
            'category_id' => 22,
        ]);
    }
}
