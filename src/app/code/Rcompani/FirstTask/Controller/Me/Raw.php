<?php

namespace Rcompani\FirstTask\Controller\Me;

use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\ResultFactory;

class Raw extends Action

{
    public function execute()
    {
        $page = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $request = $this->getRequest();
        $category = $request->getParam('category_id');
        $limit = $this->getRequest()->getParam('limit');
        $page->setContents("category_id" $category,"limit $limit");
        return $page;
    }
}
