<?php

namespace Rcompani\FirstTask\Controller\Sell;

use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

class index extends Action

{
    protected $customerSession;

    public function __construct(
        Session $customerSession,
        Context $context
    )
    {
        $this->customerSession = $customerSession;
        parent::__construct($context);
    }

    public function execute()
    {
        $page = $this->resultFactory->create(ResultFactory::TYPE_RAW);
        $customerId = $this->customerSession->getCustomerId();
        $page->setContents("customer_id: $customerId");
        return $page;
    }
}
