<?php

namespace Rcompani\FirstTask;

use Magento\Framework\Component\ComponentRegistrar;

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Rcompani_FirstTask',
    __DIR__,
);
