<?php

namespace Rcompani\FirstTask\Model\ResourceModel\Customer;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Rcompani\FirstTask\Model\Customer;

class Collection extends AbstractCollection

{
    protected function _construct()
    {
        $this->_init(Customer::class,\Rcompani\FirstTask\Model\ResourceModel\Customer::class);
    }
}

