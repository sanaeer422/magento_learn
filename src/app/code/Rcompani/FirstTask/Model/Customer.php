<?php declare(strict_types=1);

namespace Rcompani\FirstTask\Model;

use Magento\Framework\Model\AbstractModel;

class Customer extends AbstractModel

{
    protected function _construct()
    {
        $this->_init(ResourceModel\Customer::class);
    }

}
