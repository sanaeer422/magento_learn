<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 2021-06-30
 * Time: 13:32
 */
namespace Rcompani\BestSelling\Controller\Category;

use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\DB\Select;
use Rcompani\BestSelling\Model\ResourceModel\Sales\Bestsellers;


use Magento\Sales\Model\ResourceModel\Report\Bestsellers\Collection as BestsellersCollection;
use Magento\Sales\Model\ResourceModel\Report\Bestsellers\CollectionFactory as BestsellersCollectionFactory;

class Index extends Action

{
    protected $bestsellersCollectionFactory;

    public function __construct(
        Context $context,
        BestsellersCollectionFactory $bestsellersCollectionFactory
    ) {
        $this->bestsellersCollectionFactory = $bestsellersCollectionFactory;
        parent::__construct($context);
    }
    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $result = $this->resultFactory->create(ResultFactory::TYPE_RAW);
        /** @var Request $request */
        $request = $this->getRequest();
        $categoryid = $request->getparam('category_id');
        $limit = $request->getparam('limit');

        /** @var $bestsellersCollection BestsellersCollection */
        $bestsellersCollection = $this->bestsellersCollectionFactory->create();
        $macademyBestsellersTable = Bestsellers::MAIN_TABLE;
        $bestsellersCollection->getSelect()
            ->joinLeft(
                $macademyBestsellersTable,
                "sales_bestsellers_aggregated_yearly.id = $macademyBestsellersTable.id",
                ['is_featured' => "SUM($macademyBestsellersTable.is_featured)"]
            )
            ->reset(Select::ORDER)
            ->order('is_featured DESC')
            ->order('qty_ordered DESC');
        $allItems = $bestsellersCollection->getItems();
        echo '<pre>';
        foreach ($allItems as $item) {
            var_dump($item->getData());
        }
        die();

        $result->setContents("category_id $categoryid, limit: $limit");
        return $result;
    }
}
