<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 2021-06-30
 * Time: 13:32
 */
namespace Rcompani\BestSelling\Controller\Category;

use Magento\Framework\App\Action\Action;


class All extends Action


{
    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        return $this->_forward('index', null, null, [
            'limit' => 1000,
            ]
        );

    }
}
