<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 2021-06-30
 * Time: 13:32
 */
namespace Rcompani\BestSelling\Controller\ali;

use Magento\Framework\App\Action\Context;
use Magento\Framework\HTTP\PhpEnvironment\Request;
use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\ResultFactory;
use function PHPSTORM_META\type;

class Index extends Action

{

    public function execute()
    {
        $result = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $request = $this->getRequest();

        $id = $this->getRequest()->getParam('id');
        $number = $this->getRequest()->getParam('number');
        $result->setContents("number: $number, id: $id ");
        $this->resultFactory->create();

    }
}
