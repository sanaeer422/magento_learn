<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 2021-06-30
 * Time: 13:32
 */
namespace Rcompani\BestSelling\Controller\Me;

use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Controller\ResultFactory;


class Index extends Action

{
    protected $customerSession;

    public function __construct(Context $context, Session $customerSession)
    {
        $this-> customerSession = $customerSession;
        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->resultFactory->create(ResultFactory::TYPE_RAW);
        $customerid = $this->customerSession->getCustomerId();

        $this->_eventManager->dispatch('customer_views_bestselling_me_index', [
            'customer_id' => $customerid,
        ]);

        $result->setContents("customer_id : $customerid");
        return $result;
    }
}
