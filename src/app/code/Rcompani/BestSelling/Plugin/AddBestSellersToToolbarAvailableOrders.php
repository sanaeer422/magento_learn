<?php

namespace Rcompani\BestSelling\plugin;

use Magento\Catalog\Block\Product\ProductList\Toolbar;
use Magento\Reports\Block\Adminhtml\Sales\Bestsellers;

class AddBestSellersToToolbarAvailableOrders
{
    public function afterGetAvailableOrders(Toolbar $subject, $result)
    {
        $result['Bestsellers'] = 'BestSelling';
        return $result;
    }
}
