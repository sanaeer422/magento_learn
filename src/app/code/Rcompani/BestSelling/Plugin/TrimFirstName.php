<?php

namespace Rcompani\BestSelling\Plugin;

use Magento\Customer\Controller\Account\CreatePost;
use function GuzzleHttp\Psr7\str;

class TrimFirstName
{
    public function beforeExecute (CreatePost $subject)
    {
        $firstname = $subject->getRequest()->getParam('firstname');
        $firstname_trim = str_replace(' ','', $firstname);
        $subject->getRequest()->setParam('firstname', $firstname_trim);
        return  [$subject];

    }
}
