<?php

namespace Rcompani\BestSelling\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;

class Some implements ArgumentInterface
{
    public function getWithWordCount()
    {
        $some = __('Some Thing');
        $wordCount = str_word_count($some);
        return "$some <em>($wordCount words)</em>";
    }
}