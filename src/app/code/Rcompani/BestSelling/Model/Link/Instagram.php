<?php

namespace Rcompani\BestSelling\Model\Link;

use Rcompani\BestSelling\Model\ResourceModel\Link as ResourceLink;
use Magento\Catalog\Model\AbstractModel;

class Instagram extends AbstractModel

{
    protected function _construct()
    {
        $this->_init(ResourceLink::class);
    }
}
