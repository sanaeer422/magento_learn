<?php

namespace Rcompani\BestSelling\Model\Sales;

use Rcompani\BestSelling\Model\ResourceModel\Sales\BestSellers as BestsellersResourceModel;
use Magento\Framework\Model\AbstractModel;

class
BestSellers extends AbstractModel

{
    protected function _construct()
    {
        $this->_init(BestsellersResourceModel::class);
    }

}
