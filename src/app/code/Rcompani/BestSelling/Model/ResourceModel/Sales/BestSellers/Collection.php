<?php

namespace Rcompani\BestSelling\Model\ResourceModel\Sales\BestSellers;

use Rcompani\BestSelling\Model\ResourceMosel\Sales\BestSellers as BestSellersResourceModel;
use Rcompani\BestSelling\Model\Sales\BestSellers as BestSellersModel;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(BestSellersModel::class,BestSellersResourceModel::class);
    }
}

