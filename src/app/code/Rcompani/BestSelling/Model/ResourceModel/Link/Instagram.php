<?php

namespace Rcompani\BestSelling\Model\Link;

use Magento\Framework\Model\AbstractModel;

class Instagram extends AbstractDb
{
    const MAIN_TABLE = 'rcompani_instagram_link';
    const ID_FIELD_NAME = 'instagram_link';



    protected function _construct()
    {
        $this->_init( self::MAIN_TABLE,self::ID_FIELD_NAME);

    }
}
