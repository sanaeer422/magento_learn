<?php

namespace Rcompani\BestSelling\Model\ResourceModel\Link\Ins;

use Rcompani\BestSelling\Model\ResourceModel\Link as ResourceInstagramLink;
use Rcompani\BestSelling\Model\Link as ModelInstagramLink;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(ModelInstagramLink::class,ResourceInstagramLink::class);
    }
}
