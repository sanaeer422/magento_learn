<?php declare(strict_types=1);

use Magento\Framework\Component\CompoenentRegistrar;

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Rcompani_Minerva',
    __DIR__,
);
