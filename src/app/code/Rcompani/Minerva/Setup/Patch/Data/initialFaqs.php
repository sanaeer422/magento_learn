<?php declare(strict_types=1);

namespace Rcompani\Minerva\Setup\Patch\Data;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Rcompani\Minerva\Model\ResourceModel\Faq;

class initialFaqs implements DataPatchInterface
{
    /** @var ModuleDataSetupInterface */
    protected $ModuleDataSetup;
    /** @var ResourceConnection */
    protected $resource;

    /**
     * initialFaqs constructor.
     * @param ModuleDataSetupInterface $ModuleDataSetup
     * @param ResourceConnection $resource
     */
    public function __construct(
        ModuleDataSetupInterface $ModuleDataSetup,
        ResourceConnection $resource

    )
    {
        $this->ModuleDataSetup = $ModuleDataSetup;
        $this->resource = $resource;
    }

    /**
     * Get array of patches that have to be executed prior to this.
     *
     * Example of implementation:
     *
     * [
     *      \Vendor_Name\Module_Name\Setup\Patch\Patch1::class,
     *      \Vendor_Name\Module_Name\Setup\Patch\Patch2::class
     * ]
     *
     * @return string[]
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * Get aliases (previous names) for the patch.
     *
     * @return string[]
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * Run code inside patch
     * If code fails, patch must be reverted, in case when we are speaking about schema - then under revert
     * means run PatchInterface::revert()
     *
     * If we speak about data, under revert means: $transaction->rollback()
     *
     * @return $this
     */
    public function apply(): self
    {
        $connection = $this->resource->getConnection();
        $data = [
            [
                'question' => 'What is your best selling item?',
                'answer' => 'The item you buy!',
                'is_published' => 1,
            ],
            [
                'question' => 'What is your custumer support number?',
                'answer' => '0936_313_8953.Reza Sanaeefar',
                'is_published' => 1,
            ],
            [
                'question' => 'When Will I get my order?',
                'answer' => 'When it gets delivered, Silly!',
                'is_published' => 0,
            ],
        ];
        $connection->insertMultiple(Faq::MAIN_TABLE, $data);
        return $this;
    }
}

