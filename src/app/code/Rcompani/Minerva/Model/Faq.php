<?php declare(strict_types=1);

namespace Rcompani\Minerva\Model;

class Faq extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init(ResourceModel\Faq::class);
    }
}
